# Frontend ReactJS de Emissão de Token GOV BR

Este exemplo apresenta os passos necessários para obter um token de acesso ao serviço GOV BR através da autenticação com servidor OAuth2.0. 

Utiliza para demonstração o ambiente de homologação do GOV BR através do endereço https://cas.staging.iti.br/oauth2.0

Ao clicar no botão de Obter Token de Acesso o usuário é redirecionado ao servidor para autenticação com as credenciais GOV. Após autenticação do usuário, o servidor OAuth realiza redirecionamento ao frontend, que irá utilizar a variável "code" para requisitar um token de acesso do usuário. 

A duração do token é gerenciada através da data de expiração fornecida pelo servidor OAuth.
O token obtido, poderá então ser utilizado nos endpoints de assinatura que possuem suporte para uso de certificado em nuvem.

### Tech

O exemplo utiliza das bibliotecas JavaScript abaixo:
* [ReactJS] - A JavaScript library for building user interfaces!
* [Axios] - Promise based HTTP client for the browser and node.js.

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com os dados de conexão e identificação da aplicação cliente.

| Variável | Descrição | Arquivo |
| ------ | ------ | ------ |
| <uri_base> | Endereço base do servidor OAuth para obtenção de credenciais. | pages/Token/index.js
| <client_id> | **client_id** de uma aplicação. | pages/Token/index.js
| <client_secret> | **client_secret** de uma aplicação. | pages/Token/index.js

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. 

Utilizamos o ReactJS versão 16.10 para desenvolvimento e o [Npm] versão 6.13 ou o [Yarn] versão 1.22 para instalação das dependências e execução da aplicação.

Para realizar a comunicação com o servidor OAuth externo é necessário utilizar proxy para chamada a partir do frontend. A configuração do proxy está definida no arquivo setupProxy.js.

##### Comandos:

**Instalar as dependências utilizando o comando abaixo:**

    - npm install

  ou

    - yarn

**Executar programa:**

    - npm start

  ou

    - yarn start

   [Node]: <https://nodejs.org/en/>
   [ReactJS]: <https://reactjs.org/>
   [Axios]: <https://github.com/axios/axios>
   [Yarn]: <https://yarnpkg.com/>
   [Npm]: <https://www.npmjs.com/>
