import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import logo from './assets/logobry.png';
import Token from './pages/Token/index';

function App() {
// CRIA HTML BASE DE TODA APLICAÇÃO
  return (
    <div className="container">
      <img className="logo" src={logo} alt="BRy Tecnologia" />
      <div className="content">
        <BrowserRouter>
              <Switch>
                  <Route path="/" exact component={Token} />
              </Switch>
          </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
