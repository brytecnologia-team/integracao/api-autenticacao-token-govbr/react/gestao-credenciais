import React, { useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import axios from 'axios';

export default function Token() {
  //Estes valores devem ser preenchidos com as informações de acesso a API Assinatura
  const client_id = "devLocal"; 
  const client_secret = "younIrtyij3";
  const uri_base = "https://cas.staging.iti.br/oauth2.0";

  // Cria as variáveis que serão utilizadas/atualizadas na requisição
  const [novoToken, setAtualizar] = useState(false);
  const [scope, setScope] = useState("signature_session");
  const [tokenAcesso, setToken] = useState("");
  const [dataExpiracaoToken, setData] = useState(new Date());

  // Variavéis para obter e atualizar os parâmetros da query
  const history = useHistory();
  const queryParams = new URLSearchParams(useLocation().search);
  
  // Função que será executada quando for clicado no botão "Obter Token de Acesso"
  async function getCode(event) {
    event.preventDefault();
    // Verifica se um novo Token será gerado (Token não está expirado e opção de Novo Token não está marcada)
    if (! (new Date().getTime() > dataExpiracaoToken.getTime() || novoToken)) {
      console.log("Data de expiracao");
      console.log(dataExpiracaoToken.toString());
      alert("O Token obtido ainda está válido!");

      return;
    }

    // Calcula URI a qual a página será redirecionada para obtenção de "code"
    let localUri = getLocalUri();
    let uriRedirect = uri_base + '/authorize?response_type=code&redirect_uri=' + localUri + '&scope=' + scope + '&client_id=' + client_id

    // Redireciona página
    window.location.replace(uriRedirect);
  }

  // Função que retorna a URI base da página
  function getLocalUri() {
    return window.location.protocol + '//' + window.location.hostname + ":" + window.location.port + window.location.pathname;
  }

  let code = queryParams.get("code");
  // Se parâmetro "code" está presente na query
  if (code) {
    // Calcula URI a qual será utilizada para obter token de acesso
    let localUri = getLocalUri();
    let uriGet = '/token?code=' + code + '&client_id=' + client_id + '&grant_type=authorization_code&client_secret=' + client_secret + '&redirect_uri=' + localUri

    // Remove query parameter já utilizado
    queryParams.delete("code");
    history.replace({
      search: queryParams.toString(),
    });
    
    // Realiza requisição para obter token de acesso do servidor OAuth
    axios(uriGet).then(
      function(result) {
        console.log("Requisição ocorreu com sucesso!");
        console.log(result);

        // Atualiza Access Token
        let token = result.data.access_token;
        setToken(token);
        
        // Atualiza data de expiração (valor de data.expires_in está em segundos)
        let expires = result.data.expires_in * 1000;
        setData(new Date(Date.now() + expires));
      },
      function(error) {
        console.log("Um erro ocorreu ao realizar requisição.")
        console.log(error);
        alert(error.message);
      }
    );
  } 
    
  // HTML(JSX) DA PÁGINA  
    return (
      <>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <h2>Exemplo geração de Token GOVBR</h2>
      <form onSubmit={getCode}>
        <label htmlFor="scope"></label>
        <select
          name="scope"
          value={scope}
          onChange={event => setScope(event.target.value)}
        >
          <option value="signature_session">Assinatura em Lote</option>
          <option value="sign">Assinatura única</option>
        </select>

        <label htmlFor="novoToken">Sempre Gerar Novo Token? </label>
        <select
          name="novoToken"
          value={novoToken}
          onChange={event => setAtualizar(event.target.value)}
        >
          <option value="true">Sim</option>
          <option value="false">Não</option>
        </select>

        <button className="btn" type="submit">
          Obter Token de Acesso
        </button>
      </form>
      {tokenAcesso === "" ? ("") : (
        <>
          <br></br>
          <br></br>
          <h2>Token GOVBR: </h2><h3>{tokenAcesso}</h3>
        </>
        )}
    </>
  );
}
