const { createProxyMiddleware } = require('http-proxy-middleware');

//Adiciona Proxy para permitir as chamadas ao servidor OAuth
module.exports = function(app) {
  app.use(
    '/token',
    createProxyMiddleware({
      target: 'https://cas.staging.iti.br/oauth2.0',
      changeOrigin: true,
    })
  );
};